# README #
this project we had to make our own linked list class and simulate a TCP network
Name:   Luke Simmons

Course:     CS 201R
Program: 	program 7
Due Date: 	5/10/2015
Description: 	we want to write and simulate a TCP network
Inputs: 	numberoPackets
Outputs: 	output.txt
Algorithm: 	
1. make a Linked list class
1a. this class should have all of the capabilities of the real linked list class
2.make a sender and receiver class.
2a. the sender and receiver should be able to communicate with each other through another class called network.
3. the network should from time to time corrupt the data

