

/*********************************************

Name:   Luke Simmons

Course:     CS 201R
Program: 	program 7
Due Date: 	5/10/2015
Description: 	we want to write and simulate a TCP netword
Inputs: 	numberoPackets
Outputs: 	output.txt
Algorithm: 	
1. make a Linked list class
1a. this class should have all of the capabilities of the real linked list class
2.make a sender and receiver class.
2a. the sender and recevier should beable to communicate with each other through another class called network.
3. the network should from time to time coorupt the data


*********************************************/
#include "List.h"
#include "Receiver.h"
#include "Sender.h"
#include "Network.h"
#include <iostream>
#include <ctime>
#include <fstream>

using namespace std;

int main()
{
    ofstream fout("output.txt");
    int numberoPackets;
    cout << "Enter the number of packets" << endl;
    cin >> numberoPackets;
    srand(time(NULL));
    Sender S;
    S.setup(numberoPackets);
    Receiver R;
    R.setup();
    Network N;
    while (S.isfinished() == false)
    {
        S.process(fout);
        R.process(fout);
        N.pick_up(S,fout);
        N.deliver(R,fout);
        N.pick_up(R,fout);
        N.deliver(S,fout);
 
    }
}