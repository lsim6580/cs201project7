#include <iostream>
#include <string>
#pragma once
using namespace std;


template <typename T>
struct Node// this will be out nodes for the list
{
    T data;
    Node<T> *next, *prev;
    Node() { next = prev = nullptr; }
    Node(const T& item) { next = prev = nullptr; data = item; }


};
template <class T>
class List
{
private:
    
    Node<T> *head;
    Node<T> *tail;
    int count;
public:
    List();
    ~List();
    List(const List<T>& other);
    void push_back(const T& item);// adds to the back
    void push_front(const T& item);// add to the front
    void push_ordered(const T& item);// add to wherever
    void sort();// sort the list 
    void sortother(Node<T> *&H);
    void Split(Node<T> *&H,Node<T> *&P, Node<T> *& Q);
    void Merge(Node<T> *&H, Node<T> *&P, Node<T> *& Q);
    const T front();// returns the front
    const T back();// returns the back
    void pop_front();// deletes the back of the list
    void pop_back();// deletes the front of the list
    int size();// returns the size
    bool is_empty() const;
    void clear();// clears the list
    void printlist();// print the list
    const List<T>& operator =(const List<T>& other);
};


