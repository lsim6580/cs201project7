#include <iostream>
#include <fstream>
#include "List.h"
#pragma once
using namespace std;

struct Packet
{
    Packet();
    Packet(int num,char type = 'D');
    char packetType;
    int packetNum;
    const bool operator >(const Packet &other)const;
    const bool operator >=(const Packet &other)const;
    const bool operator ==(const Packet &other)const;
    const bool operator <(const Packet &other)const;
    const bool operator <=(const Packet &other)const;
    const bool operator !=(const Packet &other)const;
    friend ostream& operator <<(ostream& out, const Packet &P);
};