
#include "List.h"
#include "Packet.h"
#include "Receiver.h"
#include <iostream>
using namespace std;

Receiver::Receiver()// default constructor
{
    maxSize = 10;
    Packetspercycle = 4;
    lastAck = 0;
    finished = false;

}
bool Receiver::has_data() const// checks if the list is empty
{
    if (packetReceived.is_empty() == true)
        return false;
    else return true;
}
void Receiver::setup()//sets last Ack to zero
{
    lastAck = 0;

}
Packet Receiver::send(ostream& out)// will send our packet to the network
{
    if (abouttoSend.is_empty() == false)
    {
        Packet send = abouttoSend.front();

        abouttoSend.pop_front();
        return send;
    }
    else
    {
        Packet P;
        P.packetNum = -1;
        return P;
    }
}
void Receiver::receive(const Packet &P, ostream& out)// receives packets from the network
{
    if (P.packetNum < 1)
    {
        out << "Receiver dropping corrupted packet" << endl;
        return;
    }
    else
    {
        packetReceived.push_back(P);

    }
}
void Receiver::process(ostream& out)// processes all the data
{
    if (packetReceived.is_empty() == false)// while we have data
    {
        packetReceived.sort();
        int nextup = packetReceived.size();
        for (int i = 1; i <= nextup; i++)
        {
            if (packetReceived.front().packetNum <= lastAck && packetReceived.front().packetType != 'F')// if the packet is a duplicate
            {
                out << "Receiver dropping duplicated packet " << packetReceived.front().packetNum << endl;
                // do Nothing
            }
            else if (packetReceived.front().packetNum == (lastAck + 1) && packetReceived.front().packetType == 'D')// if the packet is next in the acknowledgement sequence
            {
                lastAck = packetReceived.front().packetNum;
                if (abouttoSend.size() < 10)
                {
                    Packet ack(packetReceived.front().packetNum, 'A');// if the packet is an acknowledgement packet
                    abouttoSend.push_back(ack);

                }
            }
            else if (packetReceived.front().packetNum == lastAck && packetReceived.front().packetType == 'F')// if the packet is a finished packet
            {
                finished = true;// update variable 
                out << "Receiver receiving end transmission notice" << endl;
                if (abouttoSend.size() < 10)
                {
                    Packet done(packetReceived.front().packetNum, 'F');
                    abouttoSend.push_front(done);
                    out << "Receiver sending FIN " << lastAck <<"(acknowledging end transmission)" << endl;
                }
            }
            else
            {
                packetReceived.push_back(packetReceived.front());
            }
            packetReceived.pop_front();


        }

    }
    if (finished == false)
    {
        out << "Receiver sending Ack " << lastAck << endl;
    }
}
