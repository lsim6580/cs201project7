#include <iostream>
#include "Sender.h"
#include "Receiver.h"
#include <vector>
#include <fstream>


class Network
{
public:
    void pick_up(Sender &S,ostream& out);
    void pick_up(Receiver &R, ostream &out);
    void deliver(Sender &S, ostream &out);
    void deliver(Receiver &S,ostream &out);
    List<Packet> toSender;
    List<Packet> toReceiver;
    vector<Packet> toProccess;
};