#include <iostream>
#include "Network.h"
 
using namespace std;

void Network::pick_up(Sender &S,ostream &out)
{
    if (S.has_data() == true)
    {
        for (int k = 0; k < 4; k++)
        {
            if (rand() % 100 < 3)
            {
                out << "Network dropping packet " << S.send().packetNum << " from sender to receiver" << endl;
                Packet Garbage;
                Garbage.packetNum = -1;
                toProccess.push_back(Garbage);
            }
            else if (rand() % 100 < 2)
            {
                toProccess.push_back(S.send());
                out << "Network corrupting packet " << toProccess.front().packetNum << " from sender to receiver" << endl;
                toProccess.front().packetNum = -1;

            }
            else if (rand() % 100 <= 33)
            {
                for (int i = 0; i < toProccess.size(); i++)
                {
                    swap(toProccess[i], toProccess[rand() % toProccess.size()]);
                }
            }
            else toProccess.push_back(S.send());

        }
        for (int i = 0; i < toProccess.size(); i++)
        {
            toReceiver.push_back(toProccess[i]);
        }
        toProccess.clear();
    }

}
void Network::pick_up(Receiver &R,ostream &out)
{
    if (R.has_data() == true)
    {
        for (int k = 0; k < 4; k++)
        {
            if (rand() % 100 < 3)
            {
                out << "Network dropping packet " << R.send().packetNum << " from receiver to sender" << endl;
                Packet Garbage;
                Garbage.packetNum = -1;
                toProccess.push_back(Garbage);
            }
        
            else if (rand() % 100 < 2)
            {
                toProccess.push_back(R.send());
                out << "Network corrupting packet " << toProccess.front().packetNum << " from receiver to sender" << endl;
                toProccess.front().packetNum = -1;

            }

            else if (rand() % 100 <= 33)
            {
                for (int i = 0; i < toProccess.size(); i++)
                {
                    swap(toProccess[i], toProccess[rand() % toProccess.size()]);
                }
            }
            else
                toProccess.push_back(R.send());
        }
        for (int i = 0; i < toProccess.size(); i++)
        {
            toSender.push_back(toProccess[i]);
        }
        toProccess.clear();
    }

}
void Network::deliver(Sender &S,ostream &out)
{

    for (int i = 0; i < toSender.size(); i++)
    {
        S.receive(toSender.front(),out);
        toSender.pop_front();
    }
    
 
    
}
void Network::deliver(Receiver &S, ostream &out)
{
    
    for (int i = 0; i < toReceiver.size(); i++)
    {
        S.receive(toReceiver.front(),out);
        toReceiver.pop_front();
    }

    
}