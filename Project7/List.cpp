
#include "List.h"
using namespace std;


template <class T>
List<T>::List()
{
    count = 0;
    head = nullptr;
    tail = nullptr;
}
template <class T>
List<T>::~List()
{
    if (head != nullptr && tail != nullptr)
        clear();
}
template <class T>
List<T>::List(const List<T>& other)
{

    head = tail = nullptr;
    *this = other;
}
template <class T>
const List<T>& List<T>::operator =(const List<T>& other)
{
    if (*this != &other)
    {
        clear();
        Node<T> *List1;
        List1 = other.head;
        while (List1 != nullptr)
        {
            push_back(*List1);
            List1 = List1->next;
        }
    }
    return *this;


}

template <class T>
void List<T>::push_back(const T& item)
{
    if (count == 0)
    {
        Node<T> *nextNode = new Node<T>(item);
        head = tail = nextNode;
        count++;
    }
    else
    {
        Node<T> *nextNode = new Node<T>(item);
        nextNode->prev = tail;
        tail->next = nextNode;
        tail = nextNode;
        count++;
    }
}
template <class T>
void List<T>::push_front(const T& item)
{
    if (count == 0)
    {
        Node<T> *nextNode = new Node<T>(item);
        head = tail = nextNode;
        count++;
    }
    else
    {
        Node<T> *nextNode = new Node<T>(item);
        nextNode->next = head;
        head->prev = nextNode;
        head = nextNode;
        count++;
    }
}
template <class T>
void List<T>::push_ordered(const T& item)
{
    if (count == 0)
        push_back(item);
    else if (count == 1)
    {
        if (item < head->data)
            push_front(item);
        else
            push_front(item);
    }
    else
    {
        Node<T> *current = head;
        if (item <= head->data)
            push_front(item);
        else if (item >= tail->data)
            push_back(item);
        else
        {
            while (current != nullptr)
            {
                if (item >= current->data && item <= current->next->data)
                {
                    Node<T> *nextNode = new Node<T>(item);
                    nextNode->prev = current;
                    nextNode->next = current->next;
                    current->next = nextNode;
                    nextNode->next->prev = nextNode;
                    count++;
                    return;
                }
                else
                    current = current->next;
            }
        }
    }

}
template <class T>
const T List<T>::front()
{
    return head->data;
}
template <class T>
const T List<T>::back()
{
    return tail->data;
}
template <class T>
void List<T>::pop_front()
{
    if (count == 0)
        return;
    else if (count == 1)
    {
        delete head;
        head = tail = nullptr;
        count--;
    }
    else
    {
        head = head->next;
        delete head->prev;
        head->prev = nullptr;
        count--;
    }

}
template <class T>
void List<T>::pop_back()
{
    if (count == 0)
        return;
    else if (count == 1)
    {
        delete tail;
        tail = head = nullptr;
    }
    else
    {
        tail = tail->prev;
        delete tail->next;
        tail->next = nullptr;
        count--;
    }
}
template <class T>
int List<T>::size()
{
	return count;
}
template <class T>
bool List<T>::is_empty()
{
    if (count == 0)
        return true;
    else
        return false;
}
template <class T>
void List<T>::clear()
{
    Node<T> *thisPointer = head;
    while (head != nullptr)
    {
        head = head->next;
        delete thisPointer;
        thisPointer = head;
    }
    tail = head;
    count = 0;
}
template<class T>
void List<T>::sort()
{
    sortother(head);

    Node<T> *thisPointer = head;//sets the new node to the head position
     
    while (thisPointer->next != nullptr)
    {
        thisPointer = thisPointer->next;
    }
    tail = thisPointer;//sets out tail 

    head->prev = nullptr;
   
    thisPointer = head;
    while (thisPointer->next != nullptr)
    {
        thisPointer->next->prev = thisPointer;
        thisPointer = thisPointer->next;
    }

}

template<class T>
void List<T>::sortother(Node<T>*& HeadS)
{
    Node<T> *List1, *List2;

    if (HeadS != nullptr && HeadS->next != nullptr) // checks for at least two items
    {
        Split(HeadS, List1, List2);
        if (List1 != nullptr && List1->next != nullptr)
            sortother(List1);
        if (List2 != nullptr && List2->next != nullptr)
            sortother(List2);
        Merge(HeadS, List1, List2);
    }
}

template<class T>
void List<T>::Split(Node<T>*& HeadS, Node<T>*& List1, Node<T>*& List2)
{
    Node<T>* temp;
    int size = 0;

    temp = HeadS;
    while (temp != nullptr)
    {
        size++;
        temp = temp->next;
    }
    size = size/2;
    temp = HeadS;
    for (int k = 0; k < size - 1; k++)
        temp = temp->next;
    List1 = HeadS;  
    List2 = temp->next;
    temp->next = nullptr;
}

template<class T>
void List<T>::Merge(Node<T>*& HeadS, Node<T>*& List1, Node<T>*& List2)
{
    Node<T> *newNode;

    if (List1->data <= List2->data)
    {
        HeadS = List1;
        List1 = List1->next;
    }
    else
    {
        HeadS = List2;
        List2 = List2->next;
    }
    newNode = HeadS;
    while (List1 != nullptr && List2 != nullptr)
    {
        if (List1->data <= List2->data)
        {
            newNode->next = List1;
            newNode = List1;
            List1 = List1->next;
        }
        else
        {
            newNode->next = List2;
            newNode = List2;
            List2 = List2->next;
        }
    }
    if (List1 == nullptr)
        newNode->next = List2;
    else
        newNode->next = List1;
}