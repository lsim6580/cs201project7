#include "Packet.h"

Packet::Packet()// default constructor
{
    packetType = 'D'; packetNum = 0;
}
Packet::Packet(int num,char type)// alternate constructor
{
    packetNum = num;
    packetType = type;
}
const bool Packet::operator >(const Packet &other)const
{
    if (packetNum > other.packetNum)
        return true;
    else return false;
}
const bool Packet::operator >=(const Packet &other)const
{
    if (packetNum >= other.packetNum)
        return true;
    else
        return false;
}
const bool Packet::operator ==(const Packet &other)const
{
    if (packetNum == other.packetNum)
        return true;
    else
        return false;
}
const bool Packet::operator <(const Packet &other)const
{
    return !operator>=(other);
}
const bool Packet::operator <=(const Packet &other)const
{
    return !operator>(other);
}
const bool Packet::operator !=(const Packet &other)const
{
    return !operator==(other);
}
ostream& operator <<(ostream& out, const Packet &P)
{
    out << P.packetType << " " << P.packetNum;
    return out;
}