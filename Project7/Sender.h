#include "List.h"
#include "Packet.h"
#include <iostream>
#include <fstream>
#pragma once
using namespace std;

class Sender
{
public:
    Sender();
    void setup(int Packets);
    bool has_data() const;
    Packet send();
    bool receive(const Packet &P,ostream &out = cout);
    void process(ostream& out = cout);
    bool isfinished();
    List<Packet> readytoSend;
    List<Packet> received;
private:
    int Maxsize;
    int Packetspercycle;
    int lastAck;
    bool finished;
    int numberofPackets;



};