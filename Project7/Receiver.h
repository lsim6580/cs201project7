
#include "List.h"
#include "Packet.h"
#include <iostream>
#include <ostream>
#pragma once
using namespace std;

class Receiver
{
public:
    Receiver();
    bool has_data()const;
    void setup();
    Packet send(ostream& out = cout);
    void receive(const Packet &P, ostream& out = cout);
    void process(ostream& out = cout);
    List<Packet> packetReceived;
    List<Packet> abouttoSend;
private:
    int maxSize;
    int Packetspercycle;
    int lastAck;
    bool finished;
 
 
    

};
