
#include "Sender.h"
#include <iostream>
using namespace std;
Sender::Sender()// default constructor
{
    Maxsize = 10;
    numberofPackets = 0;
    Packetspercycle = 4;
    lastAck = 0;
    finished = false;
}
void Sender::setup(int Packets)// initializes our out list
{
    lastAck = 0;
    numberofPackets = Packets;
    if (numberofPackets >= 10)
    {
        for (int k = 1; k < 11; k++)
        {
            readytoSend.push_back(k);
        }
    }
    else
        for (int k = 1; k <= numberofPackets; k++)
    {
            readytoSend.push_back(k);
    }
}
bool Sender::has_data() const// checks for data
{
    if (!(readytoSend.is_empty()==true))
        return true;
    else return false;
}
Packet Sender::send()// sends out the data
{
    if (!(readytoSend.is_empty() == true))
    {
        Packet send = readytoSend.front();
        readytoSend.pop_front();
        return send;

    }
    else
    {
        Packet P;
        P.packetNum = -1;
        return P;
    }
}
bool Sender::receive(const Packet &P, ostream &out)
{
    if (P.packetNum < 1)
    {
        out << "Sender dropping corrupted packet" << endl;
        return false;
    }
    else
    {

        if (received.size() < 10)
        {
            received.push_back(P);
            return true;
        }
        else
            return false;
    }



}
void Sender::process(ostream& out)
{
    //while (isfinished()!=true)//while not finished
    {
        if (!(received.is_empty() == true))// while our receiver queue is not empty
        {
            received.sort();
            int whatsleft = received.size();
            for (int k = 1; k <= whatsleft; k++)
            {
                if (received.front().packetNum > lastAck && received.front().packetType == 'A')//checks for if the packet number is less than lastAck
                {
                    lastAck = received.front().packetNum; //update our lastAck
                    
                }
                else if (received.front().packetNum == lastAck && received.front().packetType == 'F')// if the last packet is of type F
                {
                    finished = true;
                    out << "Sender receiving Acknowledgement of end transmission" << endl;
                }
                received.pop_front();//empty out our queue

            }
            out << "Sender receiving ACK " << lastAck << endl;
        }
        while ((!(readytoSend.is_empty() == true)) && readytoSend.front() <= lastAck)//get rid of the packets that we dont need
        {
            readytoSend.pop_front();

        }
        if (readytoSend.size() < 4)// if our ready to send queue has less then 4 packets in it
        {
            if (lastAck == numberofPackets)// if we are at the end
            {
                Packet last(numberofPackets);
                last.packetType = 'F';
                readytoSend.push_front(last);
            }
            else
            {
                int number = lastAck+1;
                int size = readytoSend.size();
                if (number != numberofPackets + 1)
                {
                    for (int k = 0; k < (10-size); k++)
                    {
                        if (number <= numberofPackets)
                        {
                            readytoSend.push_ordered(number);
                            number++;

                        }
                    }
                }
            }
        }
    }
}
bool Sender::isfinished()
{
    return finished;
}